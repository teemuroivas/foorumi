FoorumApp.controller('UsersController', function($scope, $location, Api, $route){
  // Toteuta kontrolleri tähän
  $scope.logIn = function() {
	  Api.login({ username: $scope.User.username, password: $scope.User.password})
	 .success(function(user){
		 $location.path(/topics/);
	 })
	 .error(function(){
		 $scope.errorMessage = 'Väärä käyttäjätunnus tai salasana!';
	 });
 }

   $scope.register = function() {
   	if ($scope.User.password != $scope.User.password_confirmation) {
			$scope.errorMessage = 'Salasana ja sen varmistus eivät täsmää!';
   	} else {
   		Api.register({ username: $scope.User.username, password: $scope.User.password })
			.success(function(user){
		 $location.path(/topics/);
	 })
	 .error(function(){
		 $scope.errorMessage = 'Käyttäjätunnus on jo käytössä!';
	 });
   	}
 }

});
