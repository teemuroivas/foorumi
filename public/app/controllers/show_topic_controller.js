FoorumApp.controller('ShowTopicController', function($scope, $routeParams, $location, Api){
  // Toteuta kontrolleri tähän
	Api.getTopic($routeParams.id).success(function(topic){
	 $scope.newMessage = {}
	 $scope.topic = topic;
	 $scope.messages = topic.Messages;
	 $scope.messages_count = $scope.messages.length;
	});
	$scope.addMessage = function() {

  	Api.addMessage({
  		title: $scope.newMessage.title,
  		content: $scope.newMessage.content
  	}, $routeParams.id).success(function(message){
			$location.path(/messages/+message.id);
		});
   };
});
