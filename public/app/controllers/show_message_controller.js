FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api, $route){
	$scope.newReply = {}
  // Toteuta kontrolleri tähän
   Api.getMessage($routeParams.id).success(function(message){
		$scope.message = message;
		$scope.replies = message.Replies;
		$scope.replies_count = $scope.replies.length;
	 });
	 	$scope.addReply = function() {

  	Api.addReply({
  		content: $scope.newReply.content
  	}, $routeParams.id).success(function(reply){
			$route.reload();
  	});
   };
});
